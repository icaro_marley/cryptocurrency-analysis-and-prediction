#  Cryptocurrency Analysis and Prediction
Tentativa de prever, com Machine Learning, a evolução dos preços de Criptomoedas.
O projeto foi abandonado. A conclusão foi de que não há associação clara entre os valores passados das moedas e seus valores futuros.

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib e Seaborn (Visualização de dados)
- Requests (Web Scraping para coletar dados das Criptomoedas)