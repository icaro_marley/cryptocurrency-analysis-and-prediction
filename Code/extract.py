# -*- coding: utf-8 -*-
"""
Created on Mon Jan  1 09:39:42 2018

@author: Icaro
"""
# -*- coding: utf-8 -*-
import requests
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
import datetime
import time
import threading

interval = 1 * 60 # 1 hour

'''
# COIN INFOS
url = 'https://min-api.cryptocompare.com/data/all/coinlist'
#https://www.cryptocompare.com/api/
# socialstats
# CoinSnapshotFullById
r = requests.get(url)
df = pd.DataFrame(r.json())
coins = df['Data'].reset_index()
coins = coins.rename(columns = {'index':'Coin'})
coins = coins[~coins['Data'].isnull()]
coins = pd.concat([coins,pd.DataFrame(list(coins['Data'].values))],axis=1)
coins.drop('Data',axis=1,inplace=True)
'''
# COIN PRICE DATA
r = requests.get('https://coinmarketcap.com/all/views/all/')
soup = BeautifulSoup(r.text,'lxml')
table = soup.find('table',id="currencies-all")
thead = table.find('thead')
columns = ['id']
for th in thead.findAll('th'):
    columns.append(th.text)
coin_data = pd.DataFrame(columns=columns)
i = 0
for tr in table.findAll('tr'):
    if not tr.find('th') is None:
        continue
    line = [tr.get('id').replace('id-','',1)]
    for td in tr.findAll('td'):
        text = td.text
        if 'currency-name' in td.get('class'):
            text = td.find('a',{'class':'currency-name-container'}).text
        text = text.replace('\t','').replace('\n','').strip()
        line.append(text)
    coin_data.loc[i] = line
    i+=1
    
    
#coin_data.to_csv('coin_data_coinmarketcap.csv')
coin_list = coin_data['id']

url = 'https://coinmarketcap.com/currencies/{}/historical-data/?start=20130428&end={}'
today = datetime.datetime.now()
y = str(today.year)
m = str(today.month)
d = str(today.day)
price_coin_data = pd.DataFrame([])
today_date = y + m + d

def search_coin_info(coin,date):
    global n_active_threads
    n_active_threads+=1
    r = requests.get(url.format(coin,date))
    soup = BeautifulSoup(r.text,'lxml')
    table = soup.find('table')
    thead = table.find('thead')
    columns = ['id']
    for th in thead.findAll('th'):
        columns.append(th.text)
    price_coin_data = pd.DataFrame(columns=columns)
    i = 0
    for tr in table.findAll('tr'):
        if not tr.find('th') is None:
            continue
        line = [coin]
        for td in tr.findAll('td'):
            text = td.text
            text = text.replace('\t','').replace('\n','').strip()
            line.append(text)
        if 'No data was found for the selected time period.' in line:
            break
        price_coin_data.loc[i] = line
        i+=1
    if len(price_coin_data) == 0: print('{}: no data'.format(coin))
    print(coin)
    n_active_threads-=1
    return price_coin_data

n_threads = 20
wait_time = 60
thread_list = []
n_active_threads = 0

class TaskThread(threading.Thread):

    def __init__(self,func,args):
        threading.Thread.__init__(self)
        self.kwargs = args
        self.func = func
        self.result = ""

    def run (self):
        try:
            self.result = self.func(**self.kwargs)
        except Exception as e:
            print("Exception ({}) at func {} args {}".format(str(e),self.func,self.kwargs))
                  
    def join(self):
        threading.Thread.join(self)
        return self.result

for coin in coin_list:
    while True:
        if n_active_threads < n_threads:
            args = {'coin':coin,'date':today_date}
            thread = TaskThread(search_coin_info, args)
            thread.start()
            thread_list.append(thread)
            break
        time.sleep(wait_time)
        
price_df_list = []
for t in thread_list:
        price_df_list.append(t.join())      
price_coin_data = pd.concat(price_df_list)
#price_coin_data.to_csv('price_coin_data_coinmarketcap_'+today_date+'.csv')

len(price_df_list)
for index,t in enumerate(price_df_list):
    if not type(t) == pd.core.frame.DataFrame:
        print(type(t),index)
#price_df_list.pop(1370)

'''
#https://coinmarketcap.com/currencies/farstcoin/historical-data/?start=20130428&end=201811

import re

string = """Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/smartcash/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'smartcash', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/poet/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'poet', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/red-pulse/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'red-pulse', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/enigma-project/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'enigma-project', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/edgeless/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'edgeless', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/blocknet/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'blocknet', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/dentacoin/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'dentacoin', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/zclassic/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'zclassic', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/bancor/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'bancor', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/cryptonex/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'cryptonex', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/nav-coin/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'nav-coin', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/quantstamp/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'quantstamp', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/dent/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'dent', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/gxshares/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'gxshares', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/monaco/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'monaco', 'date': '201811'}Exception (HTTPSConnectionPool(host='coinmarketcap.com', port=443): Max retries exceeded with url: /currencies/einsteinium/historical-data/?start=20130428&end=201811 (Caused by SSLError(SSLError("bad handshake: SysCallError(10054, 'WSAECONNRESET')",),))) at func <function search_coin_info at 0x000001F2A82269D8> args {'coin': 'einsteinium', 'date': '201811'}"""

errors_url_list = re.findall('\/currencies\/(?:(?:\d+)?[a-z]+-?)+\/historical-data\/\?start=20130428&end=\d+',string)

list(map(lambda x:'coinmarketcap.com'+x,errors_url_list))
'''

'''
add only new data
'''

import pymongo
#.\mongod.exe --dbpath "C:\Users\Icaro\Desktop\Projetos\Cryptocurrency Data Science\Data\db"
client = pymongo.MongoClient('localhost', 27017)    

db = client['coin-database']

coin_collection = db['coin_info']
coin_collection.insert_many(coins.to_dict('records'))
#coin_collection.find_one({"Name": "ZSC"})

#coin price
coin_price_collection = db['coin_price']
#coin_price_collection.insert_many(price_coin_data.to_dict('records'))





