# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import datetime
import time
import pymongo
import matplotlib.pyplot as plt
import seaborn as sns

save_path = "../Data/"

#.\mongod.exe --dbpath "C:\Users\Icaro\Desktop\Projetos\Cryptocurrency Data Science\Data\db"
# mongod --dbpath "/home/icaromarley5/Área de Trabalho/Projects/Cryptocurrency Data Science/cryptocurrency-data-science/Data/db"
client = pymongo.MongoClient('localhost', 27017)    
db = client['coin-database']

#coin price
coin_price_collection = db['coin_price']
coin_price_list = []
for doc in coin_price_collection.find():
    coin_price_list.append(doc)
coin_price_df = pd.DataFrame(coin_price_list)
del coin_price_list

def str_to_float(value):
    value = value.replace(",","")
    if value == "-":
        return np.nan
    return float(value)
coin_price_df['Close'] = coin_price_df['Close'].apply(str_to_float)
coin_price_df['Market Cap'] = coin_price_df['Market Cap'].apply(str_to_float)
coin_price_df['Volume'] = coin_price_df['Volume'].apply(str_to_float)

coin_price_df['Date_w_days'] = pd.to_datetime(coin_price_df['Date']).apply(lambda x:str(np.datetime64(x,"M")))
coin_price_df.sort_values(['id','Date_w_days'],inplace=True)
coin_price_months = coin_price_df.groupby(['id','Date_w_days'])[['Close','Market Cap','Volume']].mean().reset_index()
#del coin_price_df

# n months
coin_price_months['count'] = 1
sums = coin_price_months.groupby('id')['count'].cumsum().rename('n_months')
del coin_price_months['count'] 
coin_price_months = coin_price_months.join(sums)

# mult from first values
columns = ['Close','Market Cap','Volume']
for column in columns:
    column_first_values = coin_price_months.groupby(['id'])\
        [column].first().rename(column +'_first').reset_index()
    coin_price_months = coin_price_months.\
        merge(column_first_values,on='id')
    coin_price_months[column+'_mult'] = \
        coin_price_months[column] / \
            coin_price_months[column+'_first']
            
n_months = 3
def find_first_peak(groupby, n_months):
    groupby['Close_peak'] = False
    size = len(groupby)
    start = groupby.index[0]
    end = groupby.index[0] + size
    i = start
    peak_pos = -1
    while i < end:
        if i - start >= n_months:
            if groupby.loc[i,'Close'] > 20:
                possible_peak = groupby.loc[i,'Close_mult']
                l_inf = i - n_months
                l_sup = i -1
                #biggest_peak = groupby.loc[l_inf:l_sup,'Close_mult']\
                #    .max()
                #print("{}: {} [{},{}] {}".format(groupby['id'].unique(),possible_peak,l_inf,l_sup,biggest_peak))
                previous_months = groupby.loc[l_inf:l_sup,'Close_mult']
                if previous_months.max() < possible_peak:
                    if groupby.loc[l_inf:l_sup,'Close_mult']\
                        .max() <= possible_peak * (1 - 0.4):
                        peak_pos = i
                        break
        i += 1
    if peak_pos != -1:
        groupby.loc[peak_pos,'Close_peak'] = True
    return groupby
coin_price_months = coin_price_months.groupby('id')\
    .apply(lambda x: find_first_peak(x,n_months))
coin_price_months.Close_peak.value_counts()

'''
coins = coin_price_months[coin_price_months['Close_peak'] == True]['id'].unique()

coins= [coin for coin in coin_price_final['id'].unique() if coin not in coins]
for coin in coins:
    data = coin_price_months[coin_price_months['id'] == coin]
    if data['Close'].max() <= 30: continue
    ax = sns.pointplot(x='Date_w_days',y='Close',data=data,hue='Close_peak',join=False)
    plt.title("Coin: {}".format(coin))
    plt.xticks(rotation=45)
    plt.show()
    time.sleep(2)
'''




#---------------

# select first n months peak only
def select_first_n_months(groupby,n_months):
    del groupby['id']
    peak_list = groupby[groupby['Close_peak'] == True]
    if len(peak_list > 0):
        index_first_row = groupby.head(1).index[0]
        index_first_peak = peak_list.head(1).index[0]
        if index_first_peak - index_first_row >= n_months + 1:
            index_list = np.arange(
                index_first_peak - n_months,index_first_peak + 1)
            return groupby.loc[index_list]
        return groupby.head(0)
    # if there is no peak, return the last months of data
    if len(groupby) >= n_months + 1:
        return groupby.tail(n_months + 1)
    # if there is not enough data, don't return it
    return groupby.head(0)
coin_price_n_months = coin_price_months.groupby('id')\
    .apply(lambda x: select_first_n_months(x,n_months))\
    .reset_index()
del coin_price_n_months['level_1']
#len(check) / (n_months + 1)
      
columns = [ 'Close','Close_mult', 'Market Cap', 'Market Cap_mult',
    'Volume','Volume_mult']
def shift_month_data(groupby,columns,n_months):        
    for column in columns:
        for i in range(1,n_months+1):
            groupby[column+"_"+str(i)] =\
                groupby[column].shift(i)
    groupby['Close_peak'] = groupby['Close_peak']\
        .tail(1).values[0] == True
    return groupby
coin_price_final = coin_price_n_months.groupby('id')\
    .apply(lambda x: \
        shift_month_data(x,columns,n_months))
coin_price_final.reset_index(drop=True,inplace=True)

coin_price_final = coin_price_final.groupby('id').tail(1)

columns = np.array(coin_price_final.columns)
columns = np.delete(columns,
    [np.where(columns=='Close'),np.where(columns=='Market Cap'),np.where(columns=='Volume')])

coin_price_final = coin_price_final[columns]

#coin_price_final['Close_peak'].value_counts()

coin_price_final.to_csv(save_path+'coins.csv',index=False)


# info
coin_info_collection = db['coin_info']
coin_info_list = []
for doc in coin_info_collection.find():
    coin_info_list.append(doc)
coin_info_df = pd.DataFrame(coin_info_list)
del coin_info_list


# id standardization


coin_info_df.to_csv(save_path+'coin_aux_info.csv',index=False)

from nltk.metrics.distance import edit_distance

coins = coin_price_final['id'].unique()
for coin in coins:
    dist_list = coin_info_df['CoinName'].astype(str)\
        .apply(lambda x: edit_distance(x,coin))
    closest_coin = dist_list[dist_list.min() == dist_list].index
    closest_coin_name = coin_info_df.iloc[closest_coin]['CoinName'].values[0]
    print(coin,closest_coin_name)


